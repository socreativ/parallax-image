"use strict";

docReady(() => {

    const parallaxElements = document.querySelectorAll('.parallaxImg');
    parallaxElements.forEach((element) => {
        if(window.innerWidth > 768){
            const imgs = element.querySelectorAll('.elem-para');
            const scales = element.querySelectorAll('.elem-scale');

            // IMG 1
            gsap.to(imgs[0], {y: -25, scrollTrigger: {
                trigger: element,
                start: "top bottom",
                end: "bottom top",
                scrub: true,
            }});

            // IMG 2 
            gsap.to(imgs[1], {y: -15, scrollTrigger: {
                trigger: element,
                start: "top center",
                end: "bottom center",
                scrub: true,
            }});

            // IMG 3
            gsap.to(imgs[2], {y: 125, scrollTrigger: {
                trigger: element,
                start: "top center",
                end: "bottom center",
                scrub: true,
            }});

            gsap.to(scales, {scale: 1.2, scrollTrigger: {
                trigger: element,
                start: "top bottom",
                end: "bottom top",
                scrub: true,
            }});
        }
    });

});