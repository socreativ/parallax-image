<?php

$id = isset($block['anchor']) && $block['anchor'] ? $block['anchor'] : $block['id'];
$css = isset($block['className']) ? $block['className'] : '';

$txt = get_field('texte');
$img1 = get_field('img1');
$img2 = get_field('img2');
$img3 = get_field('img3');


?>

<section id="<?= $id ?>" class="parallaxImg <?= $css ?>">
    <div class="d-flex">
        <?php if(!my_wp_is_mobile()): ?>
            <div class="half">
                <?php if ($img1['scale']) : ?>
                    <div class="elem-para">
                <?php endif; ?>
                    <?php
                    $css = $img1['scale'] ? "elem-scale" : "elem-para text-container";
                    $img1Url = $img1['image']['url'];
                    $ext = pathinfo($img1Url, PATHINFO_EXTENSION);
                    if( $ext == 'gif' ):?>
                        <img class="<?=  $css; ?> custom-lazy" src="" custom-lazy-src="<?=  $img1Url; ?>" alt="<?=  $img1['image']['alt'] ?>">
                    <?php else:?>
                        <img class="<?=  $css; ?>" src="<?=  $img1Url; ?>" alt="<?=  $img1['image']['alt'] ?>">
                    <?php endif; ?>
                <?php if ($img1['scale']) : ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>

        <div class="half">
            <p><?=  $txt ?></p>
            <?php if(!my_wp_is_mobile()): ?>
                <div class="position-relative">
                <?php if ($img2['scale']) : ?>
                    <div class="elem-para position-relative position-md-absolute z-9 overflow-hidden text-container">
                <?php endif; ?>
                    <?php
                    $css = $img2['scale'] ? "  elem-scale" : "elem-para text-container ";
                    $img2Url = $img2['image']['url'];
                    $ext = pathinfo($img2Url, PATHINFO_EXTENSION);
                    if( $ext == 'gif' ):?>
                        <img class="<?=  $css; ?> custom-lazy" src="" custom-lazy-src="<?=  $img2Url; ?>" alt="<?=  $img2['image']['alt'] ?>">
                    <?php else:?>
                        <img class="<?=  $css; ?>" src="<?=  $img2Url; ?>" alt="<?=  $img2['image']['alt'] ?>">
                    <?php endif; ?>
                <?php if ($img2['scale']) : ?>
                    </div>
                <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>

    </div>

        <div class="full">
            <?php if ($img3['scale']) : ?>
                <div class="elem-para position-relative position-md-absolute z-9 overflow-hidden text-container">
            <?php endif; ?>
                <?php
                $css = $img3['scale'] ? "  elem-scale" : "elem-para text-container ";
                $img3Url = $img3['image']['url'];
                $ext = pathinfo($img3Url, PATHINFO_EXTENSION);
                if( $ext == 'gif' ):?>
                    <img class="<?=  $css; ?> custom-lazy" src="" custom-lazy-src="<?=  $img3Url; ?>" alt="<?=  $img3['image']['alt'] ?>">
                <?php else:?>
                    <img class="<?=  $css; ?>" src="<?=  $img3Url; ?>" alt="<?=  $img3['image']['alt'] ?>">
                <?php endif; ?>
            <?php if ($img3['scale']) : ?>
                </div>
            <?php endif; ?>
        </div>
</section>
